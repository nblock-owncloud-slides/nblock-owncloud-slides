Inhalt
======
- Einleitung
  ? Was ist OC
  + Historie
  + Aktivität im Projekt
  + Dependencies
  + Community vs. Enterprise
    + Lizenz
    + Unterschiede
    + ...

- Überblick über die Features
  - Files
  - Calendar
  - Contacts
  - Documents
  - Sync?
  - …

- Clients
  - Mobile clients
  - Desktop clients

- Security
- Fazit
- Demo

# vim: set fdm=marker fdl=1 ts=2 sw=2 et:
